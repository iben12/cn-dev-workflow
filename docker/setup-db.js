db = connect("localhost:27017/dev");

db.createUser({
  user: 'dev',
  pwd: 'dev',
  roles: [{ role: 'readWrite', db: 'dev' }]
});