db = connect("localhost:27017/test");

db.createUser({
  user: 'dev',
  pwd: 'dev',
  roles: [{ role: 'readWrite', db: 'test' }]
});