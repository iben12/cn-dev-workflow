const mongoose = require('mongoose');
const config = require('./config');
const logger = require('./lib/logger/logger').create('db: ');

module.exports = {
  connect: () => {
    mongoose.connect(
      config.db.connect,
      {
        useNewUrlParser: true
      }
    );
    mongoose.set('useCreateIndex', true);
    mongoose.set('useFindAndModify', false)

    const db = mongoose.connection;
    console.log(`Mongoose is connecting ${db.name} at ${db.host}:${db.port}\n`);
    db.on('error', error => logger.error('mongoose error', { error: error.message }));
  }
};
