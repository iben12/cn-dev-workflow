const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ScheduleSchema = new Schema({
dependentName: {
    type: String,
    required: true
},
dependent: { 
    type: Schema.Types.ObjectId,
    ref: 'Dependent'
  },
family: {
    type: Schema.Types.ObjectId,
    ref: 'Family'
},
schedule: [{ 
        eventTitle: {
          type: String,
          required: true
        },
        eventStartDate: {
          type: Date,
          required: true
        },
        eventEndDate: {
          type: Date,
          required: true
        },
        eventLocation: {
          type: String
        },
        eventDetails: {
            type: String
          },
        camp: {
            type: Schema.Types.ObjectId,
            ref: 'Camp'
        },
        /* //ez nem biztos hogy kell
        campSession: {
            type: Schema.Types.ObjectId,
            ref: 'campSession'
        }, */
        eventState: { //státusz: pl.: lefoglalva, kifizetve, leelőlegezve, stb.
          type: String
        },
        labels: [ //az event kártyákhoz hozzáadható  cimkék, még az enumot nem gondoltam végig :)
          {
           type: String
          }
        ],
        sharedEvent: { //közös program, az egész családra érvényes, minden gyerek naptárában ugyan az.
            type: Boolean,
            default: false
        }    
      }]
    });
      
module.exports = mongoose.model('Schedule', ScheduleSchema);