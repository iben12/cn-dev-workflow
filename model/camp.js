const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CampSchema = new Schema({
  
    published: { 
    type: Boolean,
    default: false
  },
  year: {
    type: Number,
    required: true
  },  
  title: {
    type: String,
    required: true
  },
  campType: { 
    type: String,
    required: true
    //, Enum: TBD: napközis, ottalvós, félnapos, egyéb
  },
  description: {
    type: String,
    required: true
  },
  categoryTags: [ {
    tag: {
        type: String,
        required: true
   } }
  ],
  recommendedAgeFrom: {
    type: int,
    required: true
  },
  recommendedAgeTo: {
    type: int,
    required: true
  },
  locationCountry: {
    type: String,
    required: true
  },
  locationCity: {
    type: String,
    required: true
  },
  locationCounty: {
    type: String,
    required: false
  },
  locationAddress: {
    type: String,
    required: true
  },
  locationGPS: { //lat, long
    type: String,
    required: false
  },
  mainImage: {
    type: String,
    default: "default-camp-main.jpg"
  },
  campImages: [{
    image: {
      type: String
    }
  }],
  campVideos: [{
    videoURL: {
      type: String
    }
  }],
  website: {
    type: String
  },
  facebook: {
    type: String
  },
  publicEmail: { //public on listing page
    type: String
  },
  contactEmail: { //for notifications
    type: String,
    requred: true
  },
  publicPhoneNumber: { //public on listing page
    type: String
  },
  contactPhoneNumber: {
    type: String,
    required: true
  },
  defaultCampStaff:  {
    type: Schema.Types.ObjectId,
    ref: 'CampStaff'
  },
  campSessions: [{
    campSession: {
      type: Schema.Types.ObjectId,
      ref: 'CampSession' 
    },
    startDate: {
      type: Date,
      required: true
    },
    endDate: {
      type: Date,
      required: true
    },
    published: {
      type: Boolean,
      default: false
    }

  }],
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model('Camp', CampSchema);
