const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const FamilySchema = new Schema({
  title: {
    //el kell nevezni a családot, spec 1.5 pontja miatt.
    type: String,
    required: true
  },
  city: {
    type: String,
    required: true
  },
  relatives: [
    //hozzátartozók listája
    {
      relative: {
        type: Schema.Types.ObjectId,
        ref: 'User'
      },
      name: {
        type: String,
        required: true
      },
      role: {
        type: String,
        required: true // enum: ["Szülő","Nagyszülő", "Rokon"]
      },
      main: {
        //ha az aki létrehozta a család profilt akkor true
        type: Boolean,
        default: false
      },
      editor: {
        //tudja szerkeszteni a családi profilt vagy csak viewer. csak a main user tudja beállítani,és main user esetében ez true értéket kell kapjon a létrehozáskor
        type: Boolean,
        default: false
      },
      inviteSent: {
        type: Boolean,
        default: false
      }
    }
  ],
  dependents: [
    // gyerekek listája
    {
      dependent: {
        //nem vagyok benne biztos, hogy külön is kell tárolni a gyerekeket -> de kell, mert a táborszervező majd meg fogja nézni, hogy ki vesz részt a táborán.
        type: Schema.Types.ObjectId,
        ref: 'Dependent'
      },
      name: {
        type: String,
        required: true
      },
      gender: {
        type: String,
        required: true,
        enum: ['Fiú', 'Lány'] //itt gondolom valami kódokat kéne inkább használni...
      },
      dateOfBirth: {
        type: Date,
        required: true
      },
      interests: [
        {
          type: String
        }
      ],
      color: {
        //különböző szinekkel tudjuk jelölni a gyerekeket a családban
        type: String,
        required: false
      },
      photoUrl: {
        type: String,
        default: 'default.jpg'
      },
      schedule: {
        type: Schema.Types.ObjectId,
        ref: 'Schedule'
      }
    }
  ],

  date: {
    type: Date,
    default: Date.now
  },
  archived: {
    //ha törölt akkor true. Ez csak akkor kell, ha akarunk logikai törlést. 
    type: Boolean,
    default: false
  },
  archiveDate: {
    //ha akarunk logikai törlést, akkor ez a törlés dátuma. És akkor majd ez alapján a dátum alapján egy job tudja takarítani a törölt tételeket.
    type: Date
  }
});

module.exports = mongoose.model('Family', FamilySchema);
