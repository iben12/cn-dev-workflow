const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CampStaffSchema = new Schema({
camp: {
    type: Schema.Types.ObjectId,
    ref: 'Camp'
},
campSession: {
    type: Schema.Types.ObjectId,
    ref: 'CampSession'
},
campTitle: {
    type: String,
    required: true
},
campStaff: [
    {
      staffMember: {
        type: Schema.Types.ObjectId,
        ref: 'User'
      },
      isCampOwner: {
        type: Boolean,
        default: false
      },
      name: {
        type: String,
         required: true
      },
      photoUrl: {
        type: String,
         default: "default.jpg"
      },
      role: {
        type: String,
         required: true
         //enum: TBD,
      },      
      isCreator: {
        type: Boolean,
        default: false
      },
      editor: { // can edit the camp data
        type: Boolean,
        default: false
      },
      invited: {
        type: Boolean,
        default: false
      },
      acceptedInvite: {
        type: Boolean,
        default: false
      }
    }]
});

module.exports = mongoose.model('CampStaff', CampStaffSchema);