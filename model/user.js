const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const UserSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    unique: true,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  photoUrl: {
    type: String,
    default: 'default.jpg'
  },
  date: {
    type: Date,
    default: Date.now
  },
  added: {
    //ha másik user hozta létre hozzáadással(család) akkor true
    type: Boolean,
    default: false
  },
  invited: {
    //ha meghívott user akkor true
    type: Boolean,
    default: false
  },
  invitedBy: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  acceptedInvite: {
    //ha elfogadta a meghívást akkor true
    type: Boolean,
    default: false
  }
});

UserSchema.methods.toJSON = function() {
  let obj = this.toObject();
  delete obj.password;
  delete obj.__v;
  return obj;
 }

module.exports = mongoose.model('User', UserSchema);