const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const DependentSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  ssn: { //social security number = taj szám
    type: String,
    required: true
  },  
  gender: {
    type: String,
    required: true,
    enum: ["Fiú", "Lány"]
  },
  birthDate: {
    type: Date,
    required: true
  },
  allergies: [
    {
     type: String
    }
  ],
  interests: [
    {
     type: String
    }
  ],
  photoUrl: {
    type: String,
    default: "default.jpg"
  },
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model('Dependent', DependentSchema);
