const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CampSessionSchema = new Schema({
    campTitle: {
        type: String,
        required: true
    },
    camp: {
        type: Schema.Types.ObjectId,
        ref: 'Camp'
    },
    sessionStaff:  {
        type: Schema.Types.ObjectId,
        ref: 'CampStaff'
      },
    startDate: {
        type: Date,
        required: true
    },
    endDate: {
        type: Date,
        required: true
    },
    capacity: {
        type: Number,
        required: true
        //must be > 0
    },
    expandabable: {
        type: Boolean,
        default: false
    },
    reservedNumber: {
        type: Number,
        default: 0
    },
    state: {
        type: String,
        default: "draft"
        //enum: TBD, eg.: full, canceled...
    },
    isOpen: {
        type: Boolean,
        default: false
    },
    participants: [{
        child: {
            type: Schema.type.ObjectId,
            ref: 'Dependent'
        },
        family: {
            type: Schema.type.ObjectId,
            ref: "Family"
        },
        name: {
            type: String,
            required: true
        },
        gender: {
            type: String,
            required: true
            //enum: TBD
        },
        dateofBirth: {
            type: Date,
            required: true
        }
    }]
});


module.exports = mongoose.model('CampSession', CampSessionSchema);