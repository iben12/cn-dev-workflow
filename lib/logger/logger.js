'use strict';

const Logger = require('heroku-logger').Logger;

const appLogger = function(prefix) {
  this.logger = new Logger({ prefix });
}

appLogger.prototype.debug = function(message, data) {
  this.logger.debug(message, data);
}

appLogger.prototype.info = function(message, data) {
  this.logger.info(message, data);
}

appLogger.prototype.warn = function(message, data) {
  this.logger.warn(message, data);
}

appLogger.prototype.error = function(message, data) {
  this.logger.error(message, data);
}

module.exports = appLogger;


module.exports.create = (prefix) => {
  return new appLogger(prefix);
}
