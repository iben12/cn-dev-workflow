'use strict';

const jwt = require('jsonwebtoken');
const config = require('../../config');

const sign = data => {
  return jwt.sign(data, config.secret, {
    expiresIn: config.expiration || '1d'
  })
}

const verify = async req => {
  return new Promise((resolve, reject) => {
    if (!req.headers.authorization) {
      return reject(new Error('missing authorization header'));
    }
    const authHeader = req.headers.authorization.split(' ');
    const token = authHeader[1] ? authHeader[1] : authHeader[0];
    jwt.verify(token, config.secret, (e, decoded) => {
      if (e) {
        return reject(e);
      } else {
        return resolve(decoded);
      }
    });
  });
}

module.exports = {
  sign,
  verify
}
