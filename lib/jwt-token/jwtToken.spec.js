'use strict';

const jwt = require('jsonwebtoken');
const config = require('./../../config');
const jwtToken = require('./jwtToken');

describe('jwtToken', function() {
  describe('#sign', function() {
    it('should generate jwt token', function(done) {
      const data = { some: 'data' };
      const token = jwtToken.sign(data);

      jwt.verify(token, config.secret, (e, decoded) => {
        expect(decoded).to.containSubset(data);
        done();
      });
    });
  });
  describe('#verify', function() {
    it('should return decoded token if token is valid', async function() {
      const token = jwt.sign({ id: 1 }, config.secret, {
        expiresIn: '1h'
      });
      const req = {
        headers: { authorization: token }
      };

      const decoded = await jwtToken.verify(req);

      expect(decoded.id).to.be.equal(1);
    });

    it('should extract token from header', async function() {
      const token = jwt.sign({ id: 1 }, config.secret, {
        expiresIn: '1h'
      });
      const req = {
        headers: { authorization: `Bearer ${token}` }
      };

      const decoded = await jwtToken.verify(req);

      expect(decoded.id).to.be.equal(1);
    });

    it('should reject if auth header is missing', async function() {
      const req = {
        headers: {}
      };

      try {
        await jwtToken.verify(req);
      } catch (e) {
        expect(e.message).to.be.equal('missing authorization header');
      }
    });

    it('should reject if signature is invalid', async function() {
      const token = jwt.sign({ id: 1 }, 'other.secret', {
        expiresIn: '1h'
      });
      const req = {
        headers: { authorization: token }
      };

      try {
        await jwtToken.verify(req);
      } catch (e) {
        expect(e.message).to.be.equal('invalid signature');
      }
    });

    it('should reject if token is expired', async function() {
      const token = jwt.sign({ id: 1, exp: 1538731386 }, config.secret);
      const req = {
        headers: { authorization: token }
      };

      try {
        await jwtToken.verify(req);
      } catch (e) {
        expect(e.message).to.be.equal('jwt expired');
      }
    });
  });
});
