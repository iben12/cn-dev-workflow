'use strict';

const jwtToken = require('../jwt-token/jwtToken');
const User = require('./../../model/user');
const logger = require('./../logger/logger').create('authMiddleware: ');

module.exports.authenticate = async (req, res, next) => {
  let token;
  try {
    token = await jwtToken.verify(req);
  } catch (e) {
    logger.info('Invalid token', { error: e.message })
    return res.status(401).json({ error: 'Missing or invalid authorization'});
  }
  try {
    const user = await User.findById(token.id);
    if (user) {
      req.user = user;
      return next();
    } else {
      logger.info('Invalid user id', { id: token.id });
      return res.status(401).json({ error: 'Authentication failed' });
    }
  } catch (e) {
    logger.error('Unknown error happened', { error: e.message });
    next(e);
  }
};
