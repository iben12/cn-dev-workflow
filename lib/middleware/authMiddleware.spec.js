'use strict';

const User = require('./../../model/user');
const jwtToken = require('../jwt-token/jwtToken');
const authMiddleware = require('./authMiddleware').authenticate;

let userId = '5ba56757bfc4ac76429c1d26';

describe('Auth Middleware', function() {
  let req = {};
  let res = {};
  let next;

  beforeEach(function() {
    Object.assign(res, {
      status: this.sandbox.stub().returns(res),
      json: this.sandbox.stub().returns(res)
    });
    next = this.sandbox.spy();
  });

  it('should call next if token and user are valid', async function() {
    this.sandbox.stub(jwtToken, 'verify').resolves({ id: userId });
    this.sandbox.stub(User, 'findById').resolves({ _id: userId });

    await authMiddleware(req, res, next);

    expect(next).to.be.called;
    expect(req.user._id).to.be.eql(userId);
  });

  it('should return 401 response if token is invalid', async function() {
    this.sandbox.stub(jwtToken, 'verify').rejects();

    await authMiddleware(req, res, next);

    expect(res.status).to.have.been.calledWith(401);
    expect(res.json).to.have.been.calledWith({ error: 'Missing or invalid authorization'});
    expect(next).to.not.have.been.called;
  });

  it('should return 401 response if user does not exsist', async function() {
    this.sandbox.stub(jwtToken, 'verify').resolves({ id: '5ba56757bfc4ac76429c1d26' });
    this.sandbox.stub(User, 'findById').resolves(undefined);

    await authMiddleware(req, res, next);

    expect(res.status).to.have.been.calledWith(401);
    expect(res.json).to.have.been.calledWith({ error: 'Authentication failed'});
    expect(next).to.not.have.been.called;
  });
});