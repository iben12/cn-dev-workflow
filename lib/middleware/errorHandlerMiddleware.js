'use strict';

const logger = require('./../logger/logger').create('app: ');

module.exports = (error, req, res, next) => {
  if (error) {
    if (error !== 'handled') {
      logger.error(error.message, { request: `${req.method} ${req.path}`, error: JSON.stringify(error.stack) });
    }
    return res.status(500).json({ error: 'Server error' });
  }

  return next();
};
