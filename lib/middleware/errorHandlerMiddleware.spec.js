'use strict';

const errorHandler = require('./errorHandlerMiddleware');

describe('Error Handler Middleware', function() {
  let req;
  let res = {};
  let next;
  let error;

  beforeEach(function() {
    const sandbox = this.sandbox;
    Object.assign(res, {
      status: sandbox.stub().returns(res),
      json: sandbox.stub().returns(res)
    });
    next = sandbox.spy();
    error = new Error('bad error');
  });

  it('should call next if no error happened', function() {
    errorHandler(undefined, req, res, next);

    expect(next).to.have.been.called;
  });

  it('should send error response if error happened', function() {
    req = { path: '/some/path'};
    errorHandler(error, req, res, next);

    expect(res.status).to.have.been.calledWith(500);
  });
});