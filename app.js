'use strict';

const express = require('express');
const db = require('./db');
const app = express();
const bodyParser = require('body-parser');
const errorHandler = require('./lib/middleware/errorHandlerMiddleware');

const greetingController = require('./api/greeting/greetingController');
const authController = require('./api/auth/authController');
const userController = require('./api/user/userController');

db.connect();

// Request middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Routes
app.use(greetingController);
app.use('/auth', authController);
app.use(userController);

// Response middleware
app.use(errorHandler);

module.exports = app;
