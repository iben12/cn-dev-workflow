'use strict';

require('dotenv').config();

module.exports = {
  port: process.env.PORT || '8080',
  secret: process.env.APP_SECRET || 'verysecret',
  expiration: process.env.TOKEN_EXPIRATION || '1d',
  db: {
    connect: process.env.NODE_ENV === 'test' ? process.env.TEST_MONGODB_URI : process.env.MONGODB_URI
  }
};
