FROM node:alpine

ARG NODE_ENV

WORKDIR /app

COPY package.json package.json

RUN npm i

COPY . /app

CMD [ "node", "server.js" ]