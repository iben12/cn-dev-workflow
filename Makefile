t:
	DOCKER_BUILDKIT=1 docker build -t iben12/cn-gitlab-demo .
	docker-compose run app npm t

start-dev:
	DOCKER_BUILDKIT=1 docker build -t iben12/cn-gitlab-demo .
	docker-compose up

start-db:
	docker-compose up -d db