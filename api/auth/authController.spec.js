'use strict';

const jwt = require('jsonwebtoken');
const config = require('./../../config');
const bcrypt = require('bcryptjs');

const User = require('./../../model/user');

const testUser = {
  id: '5ba56757bfc4ac76429c1d26',
  name: 'Test User',
  email: 'test@email.com',
  password: 'secret'
};

describe('authController', function() {
  describe('/register', function() {
    it('should return auth token if submission is valid', async function() {
      const userSaveStub = this.sandbox.stub(User.prototype, 'save').callsFake(function() {
        this._id = testUser.id;
      });
      const response = await this.api.request
        .post('/auth/register')
        .send({ email: testUser.email, name: testUser.name, password: testUser.password });

      expect(response.status).to.equal(200);
      expect(response.body.auth).to.be.true;
      expect(response.body).to.have.property('token');
      expect(userSaveStub).to.have.been.called;
      const decoded = await jwt.verify(response.body.token, config.secret);
      expect(decoded.id).to.be.equal(testUser.id);
    });

    it('should return error on invalid submission', async function() {
      const response = await this.api.request
        .post('/auth/register')
        .send({ email: testUser.email, name: 'Test User' });

      expect(response.status).to.equal(400);
      expect(response.body.name).to.equal('ValidationError');
      expect(response.body.errors).to.containSubset({
        password: {
          kind: 'required'
        }
      });
    });
  });

  describe('/login', function() {
    it('should return valid token if credentials are valid', async function() {
      const userFindOneStub = this.sandbox.stub(User, 'findOne').resolves(testUser);
      const bcryptCompareStub = this.sandbox.stub(bcrypt, 'compare').resolves(true);

      const response = await this.api.request.post('/auth/login').send({ email: testUser.email, password: testUser.password });

      expect(userFindOneStub).to.have.been.called;
      expect(bcryptCompareStub).to.have.been.called;
      expect(response.status).to.be.equal(200);
      expect(response.body.auth).to.be.true;
      expect(response.body).to.have.property('token');

      const decoded = jwt.verify(response.body.token, config.secret);
      expect(decoded.id).to.be.equal(testUser.id);
    });

    it('should return unauthorized error if password is not valid', async function() {
      this.sandbox.stub(User, 'findOne').resolves(testUser);
      this.sandbox.stub(bcrypt, 'compare').resolves(false);

      const response = await this.api.request
        .post('/auth/login')
        .send({ email: testUser.email, password: 'invalid' });

      expect(response.status).to.be.equal(401);
    });
    
    it('should return unauthorized error if password is not valid', async function() {
      this.sandbox.stub(User, 'findOne').resolves();

      const response = await this.api.request
        .post('/auth/login')
        .send({ email: testUser.email, password: 'invalid' });

      expect(response.status).to.be.equal(401);
    });
  });
});
