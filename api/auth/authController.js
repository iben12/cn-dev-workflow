'use strict';

const express = require('express');
const router = express.Router();
const User = require('./../../model/user');
const jwtToken = require('../../lib/jwt-token/jwtToken');
const bcrypt = require('bcryptjs');
const logger = require('./../../lib/logger/logger').create('authController: ');

router.post('/register', async (req, res, next) => {
  const user = new User({
    name: req.body.name,
    email: req.body.email,
    password: req.body.password
  });
  const validationError = user.validateSync();

  if (validationError) {
    return res.status(400).send(validationError);
  }
  user.password = await bcrypt.hash(req.body.password, 8);
  try {
    await user.save();
    return res.status(200).send({ auth: true, token: jwtToken.sign({ id: user._id }) });
  } catch (e) {
    logger.error(e.message, e);
    return next('handled');
  }
});

router.post('/login', async (req, res) => {
  const user = await User.findOne({ email: req.body.email });
  const passwordIsValid =
    user
    && req.body.password
    && await bcrypt.compare(req.body.password, user.password);

  if (!passwordIsValid) {
    logger.info('invalid login attempt', { email: req.body.email, password: req.body.password ? true : false });
    return res.status(401).send({error: 'Invalid credentials'});
  }

  return res.status(200).send({ auth: true, token: jwtToken.sign({ id: user.id }) });
});

module.exports = router;
