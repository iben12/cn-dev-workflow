'use strict';

const User = require('../../model/user');

describe('userController', function() {
  let jwtVerifyStub;
  beforeEach(function() {
    jwtVerifyStub = this.stubJwtVerify();
  })
  afterEach(async function() {
    expect(jwtVerifyStub).to.have.been.called;
  });

  describe('GET /profile', function() {
    it('should return user object without password', async function() {
      const response = await this.api.request
        .get('/profile');

      expect(response.status).to.equal(200);
      expect(response.body.user).to.containSubset({
        email: this.user.email,
        name: this.user.name,
        _id: this.user.id
      });
    });
  });

  describe('POST /profile', function() {
    it('should save user updates and respond with success', async function() {
      const response = await this.api.request
        .post('/profile')
        .send({ name: 'New Name' });

      expect(response.status).to.equal(200);

      const dbUser = await User.findById(this.user.id);
      expect(dbUser.name).to.equal('New Name');
    });

    it('should return error if user is not valid', async function() {
      const response = await this.api.request
        .post('/profile')
        .send({ name: undefined, valami: 1 });

      expect(response.status).to.equal(400);
      expect(response.body.error).to.include('Validation');
    });
  });
});
