'use strict';

const express = require('express');
const router = express.Router();
const User = require('./../../model/user');
const authMiddleware = require('./../../lib/middleware/authMiddleware');

router.use(authMiddleware.authenticate);

router.get('/profile', (req, res) => {
  return res.status(200).json({ user: req.user });
});

router.post('/profile', async (req, res, next) => {
  const user = req.user;
  try {
    await User.findOneAndUpdate({ _id: user.id }, {
      name: req.body.name
    }, { runValidators: true });
    return res.status(200).send('OK');
  } catch (e) {
    if (e.name === 'ValidationError') {
      return res.status(400).json({ error: e.message });
    }
    return next(e);
  }

});

module.exports = router;
