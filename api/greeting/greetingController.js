'use strict';

const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
  return res.status(200).json({ message: 'Hello Docker Meetup!' });
});

module.exports = router;