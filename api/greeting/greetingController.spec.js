'use strict';

describe('GET /', function() {
  it('should return greeting message', async function() {
    const response = await this.api.request
      .get('/');

    expect(response.status).to.equal(200);
    expect(response.body).to.eql({
      message: 'Hello Docker Meetup!'
    });
  });
});