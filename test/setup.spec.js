'use strict';

const chai = require('chai');
const subset = require('chai-subset');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const request = require('supertest');
const Logger = require('./../lib/logger/logger');
const app = require('./../app');
const db = require('./../db');
const jwtToken = require('../lib/jwt-token/jwtToken');
const User = require('./../model/user');

global.expect = chai.expect;

chai.use(subset);
chai.use(sinonChai);

let userData = {
  photoUrl: 'default.jpg',
  added: false,
  invited: false,
  acceptedInvite: false,
  name: 'Test User',
  email: 'test@example.com',
  password: 'hashedpassword'
};

before(async function() {
  this.app = app;
  this.api = {
    create: request
  };
  this.db = db;
  this.sandbox = sinon.createSandbox();
  this.user = await User.create(userData);
});

beforeEach(function() {
  this.loggerStub = this.sandbox.stub();
  this.sandbox.stub(Logger.prototype, 'info').returns(this.loggerStub);
  this.sandbox.stub(Logger.prototype, 'error').returns(this.loggerStub);
  this.sandbox.stub(Logger.prototype, 'warn').returns(this.loggerStub);
  this.sandbox.stub(Logger.prototype, 'debug').returns(this.loggerStub);
  this.stubJwtVerify = () => this.sandbox.stub(jwtToken, 'verify').resolves({ id: this.user._id });
  const apiInstance = request(app);
  this.api.request = apiInstance;
});

after(async function() {
  await User.deleteOne({ email: userData.email });
});

afterEach(async function() {
  await User.updateOne({ _id: this.user.id },  {
    name: userData.name,
    email: userData.email,
    password: userData.password,
   });
  this.sandbox.restore();
});
